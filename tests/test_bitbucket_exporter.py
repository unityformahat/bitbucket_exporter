import unittest
from vcs_exporter.bitbucket_exporter import Bitbucket
from atlassian import Bitbucket as BitbucketSession  # atlassian api tool for python
from vcs_exporter.vcs import *


class TestBitbucket(unittest.TestCase):
    def test_bitbucket_private_ctor(self):
        bitbucket = Bitbucket.get_instance()
        self.assertTrue(bitbucket.get_projects() == [] and bitbucket.get_repositories() == [] and
                        bitbucket.get_pull_requests() == [] and bitbucket.get_commits() == [] and
                        isinstance(bitbucket.get_session(), BitbucketSession))

    def test_parse_projects(self):
        projects_sample_data_1 = []
        projects_sample_data_2 = [{'key': 'MNP', 'id': 2, 'name': 'my new project',
                                 'description': 'hey thats another project!', 'public': False, 'type': 'NORMAL',
                                 'links': {'self': [{'href': 'http://52.169.187.138:7990/projects/MNP'}]}},
                                  {'key': 'UD', 'id': 1, 'name': 'unity dashboards',
                                   'description': 'this is a project made by the devops team',
                                   'public': False, 'type': 'NORMAL',
                                   'links': {'self': [{'href': 'http://52.169.187.138:7990/projects/UD'}]}}]
        bitbucket = Bitbucket.get_instance()
        bitbucket._Bitbucket__parse_projects_data(projects_sample_data_1)
        self.assertEqual(len(bitbucket.get_projects()), len(projects_sample_data_1))
        bitbucket._Bitbucket__parse_projects_data(projects_sample_data_2)
        self.assertEqual(len(bitbucket.get_projects()), len(projects_sample_data_2))

    def test_insert_project(self):
        bitbucket = Bitbucket.get_instance()
        bitbucket.insert_project(VCSProject(1, 'abc', 'abc'))
        self.assertEqual(len(bitbucket.get_projects()), 1)
        self.assertTrue(isinstance(bitbucket.get_projects()[0], tuple))
        self.assertFalse(isinstance(bitbucket.get_projects()[0], VCSProject))
        with self.assertRaises(TypeError):
            bitbucket.insert_project(1)
        with self.assertRaises(TypeError):
            bitbucket.insert_project([1])
        with self.assertRaises(TypeError):
            bitbucket.insert_project("abc")
        bitbucket._VCSTool__projects = []

    def test_parse_repositories(self):
        repositories_sample_data_1 = []
        repositories_sample_data_2 = [{'slug': 'my-new-repo', 'id': 5, 'name': 'my new repo',
                                       'description': 'thats a cool repo', 'hierarchyId': '56ba526238c0267a649a',
                                       'scmId': 'git', 'state': 'AVAILABLE', 'statusMessage': 'Available',
                                       'forkable': True, 'project': {'key': 'MNP', 'id': 2, 'name': 'my new project'}}
                                      , {'slug': 'my-new-repo', 'id': 5, 'name': 'my new repo',
                                       'description': 'thats a cool repo', 'hierarchyId': '56ba526238c0267a649a',
                                       'scmId': 'git', 'state': 'AVAILABLE', 'statusMessage': 'Available',
                                       'forkable': True, 'project': {'key': 'MNP', 'id': 2, 'name': 'my new project'}},
                                      {'slug': 'my-new-repo', 'id': 5, 'name': 'my new repo',
                                       'description': 'thats a cool repo', 'hierarchyId': '56ba526238c0267a649a',
                                       'scmId': 'git', 'state': 'AVAILABLE', 'statusMessage': 'Available',
                                       'forkable': True, 'project': {'key': 'MNP', 'id': 2, 'name': 'my new project'}},
                                      {'slug': 'my-new-repo', 'id': 5, 'name': 'my new repo',
                                       'description': 'thats a cool repo', 'hierarchyId': '56ba526238c0267a649a',
                                       'scmId': 'git', 'state': 'AVAILABLE', 'statusMessage': 'Available',
                                       'forkable': True, 'project': {'key': 'MNP', 'id': 2, 'name': 'my new project'}}
                                      ]
        bitbucket = Bitbucket.get_instance()
        bitbucket._Bitbucket__parse_repositories_data(repositories_sample_data_1)
        self.assertEqual(len(bitbucket.get_repositories()), len(repositories_sample_data_1))
        bitbucket._Bitbucket__parse_repositories_data(repositories_sample_data_2)
        self.assertEqual(len(bitbucket.get_repositories()), len(repositories_sample_data_2))

    def test_insert_repository(self):
        bitbucket = Bitbucket.get_instance()
        bitbucket.insert_repository(VCSRepository(1, 'abc', 'a-b-c', 5))
        self.assertEqual(len(bitbucket.get_repositories()), 1)
        self.assertTrue(isinstance(bitbucket.get_repositories()[0], tuple))
        self.assertFalse(isinstance(bitbucket.get_repositories()[0], VCSRepository))
        with self.assertRaises(TypeError):
            bitbucket.insert_repository(1)
        with self.assertRaises(TypeError):
            bitbucket.insert_repository([1])
        with self.assertRaises(TypeError):
            bitbucket.insert_repository("abc")
        bitbucket._VCSTool__repositories = []

    def test_parse_pull_requests(self):
        pull_requests_sample_data_1 = {}
        pull_requests_sample_data_2 = {'size': 0,
                                      'limit': 25,
                                      'isLastPage': True,
                                      'values': [{}]}
        pull_requests_sample_data_3 = {'size': 1,
                                      'limit': 25,
                                      'isLastPage': True,
                                      'values': [{'id': 4, 'title': 'My new branch',
                                                  'state': 'OPEN', 'open': True, 'closed': False,
                                                  'createdDate': 1589279436280, 'updatedDate': 1589279436280,
                                                  'fromRef': {'id': 'refs/heads/my-new-branch',
                                                              'displayId': 'my-new-branch',
                                                              'latestCommit': 'f5c2b083ef3e8bcb49241a8b9491d49addbb129b'
                                                              },
                                                  'toRef': {'id': 'refs/heads/master', 'displayId': 'master',
                                                            'latestCommit': '6a09b5e1adbfa9b06c0af1046f2ed2bedf9d556e'}
                                                     , 'author': {'user': {'name': 'admin',
                                                                           'emailAddress': 'admin@admin.com',
                                                                           'id': 1, 'displayName': 'admin',
                                                                           'active': True, 'slug': 'admin',
                                                                           'type': 'NORMAL'}}}]}

        bitbucket = Bitbucket.get_instance()
        bitbucket._Bitbucket__parse_pull_requests_data(pull_requests_sample_data_1, 1)
        self.assertEqual(len(bitbucket.get_pull_requests()), 0)
        bitbucket._Bitbucket__parse_pull_requests_data(pull_requests_sample_data_2, 1)
        self.assertEqual(len(bitbucket.get_pull_requests()), 0)
        bitbucket._Bitbucket__parse_pull_requests_data(pull_requests_sample_data_3, 1)
        self.assertEqual(len(bitbucket.get_pull_requests()), pull_requests_sample_data_3['size'])

    def test_insert_pull_request(self):
        bitbucket = Bitbucket.get_instance()
        bitbucket.insert_pull_request(VCSPullRequest(1, 'abc', datetime.datetime(2020, 5, 12, 10, 30, 36, 280000),
                                                        'abc', 'abc', 'abc', 'abc', 2))
        self.assertEqual(len(bitbucket.get_pull_requests()), 1)
        self.assertTrue(isinstance(bitbucket.get_pull_requests()[0], tuple))
        self.assertFalse(isinstance(bitbucket.get_pull_requests()[0], VCSPullRequest))
        with self.assertRaises(TypeError):
            bitbucket.insert_pull_request(1)
        with self.assertRaises(TypeError):
            bitbucket.insert_pull_request([1])
        with self.assertRaises(TypeError):
            bitbucket.insert_pull_request("abc")
        bitbucket._VCSTool__pull_requests = []

    def test_parse_commits(self):
        commits_sample_data_1 = {}
        commits_sample_data_2 = {'size': 2,
                                  'values': [{'id': '3',
                                              'displayId': '6a09b5e1adb',
                                              'author': {'name': 'admin', 'emailAddress': 'admin@admin.com', 'id': 1,
                                                         'displayName': 'admin', 'active': True, 'slug': 'admin',
                                                         'type': 'NORMAL'}, 'message': " we made a change in readme"},
                                             {'id': '4',
                                              'displayId': '12',
                                              'author': {'name': 'alon', 'emailAddress': 'admin@admin.com', 'id': 1,
                                                         'displayName': 'admin', 'active': True, 'slug': 'admin',
                                                         'type': 'NORMAL'}, 'message': "we made a change in readme 2"}

                                             ]}

        bitbucket = Bitbucket.get_instance()
        bitbucket._Bitbucket__parse_commits_data(commits_sample_data_1, 'MNP', 5, 'my-new-repo')
        self.assertEqual(len(bitbucket.get_pull_requests()), 0)
        bitbucket._Bitbucket__parse_commits_data(commits_sample_data_2, 'MNP', 5, 'my-new-repo')
        self.assertEqual(len(bitbucket.get_commits()), len(commits_sample_data_2['values']))

    def test_insert_commit(self):
        bitbucket = Bitbucket.get_instance()
        bitbucket.insert_commit(VCSCommit(1, 'abc', 'abc', 1, 2))
        self.assertEqual(len(bitbucket.get_commits()), 1)
        self.assertTrue(isinstance(bitbucket.get_commits()[0], tuple))
        self.assertFalse(isinstance(bitbucket.get_commits()[0], VCSCommit))
        with self.assertRaises(TypeError):
            bitbucket.insert_commit(1)
        with self.assertRaises(TypeError):
            bitbucket.insert_commit([1])
        with self.assertRaises(TypeError):
            bitbucket.insert_commit("abc")
        bitbucket._VCSTool__commits = []
