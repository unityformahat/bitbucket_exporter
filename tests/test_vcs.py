import unittest
from vcs_exporter.vcs import *


class TestVCSProject(unittest.TestCase):
    def test_vcs_project_ctor_missing_input_arguments(self):
        with self.assertRaises(TypeError):
            VCSProject()
        with self.assertRaises(TypeError):
            VCSProject(1)
        with self.assertRaises(TypeError):
            VCSProject(1, "AK")

    def test_vcs_project_ctor_false_project_id_input(self):
        with self.assertRaises(ValueError):
            VCSProject("blabla", "AKA", "AK")
        with self.assertRaises(TypeError):
            VCSProject(None, "AKA", "AK")


class TestVCSRepository(unittest.TestCase):
    def test_vcs_repository_ctor_missing_input_arguments(self):
        with self.assertRaises(TypeError):
            VCSRepository()
        with self.assertRaises(TypeError):
            VCSRepository(1)
        with self.assertRaises(TypeError):
            VCSRepository(1, "AK", "bla")

    def test_vcs_repository_ctor_false_repository_id_input(self):
        with self.assertRaises(ValueError):
            VCSRepository("bla", "AKA", "AK", 1)
        with self.assertRaises(TypeError):
            VCSRepository(None, "AKA", "AK", 1)

    def test_vcs_repository_ctor_false_repository_project_id_input(self):
        with self.assertRaises(ValueError):
            VCSRepository(1, "AKA", "AK", "bla")
        with self.assertRaises(TypeError):
            VCSRepository(1, "AKA", "AK", None)


class TestVCSPullRequest(unittest.TestCase):
    def test_vcs_pull_request_ctor_missing_input_arguments(self):
        with self.assertRaises(TypeError):
            VCSPullRequest()
        with self.assertRaises(TypeError):
            VCSPullRequest(1)
        with self.assertRaises(TypeError):
            VCSPullRequest(4, 'My new branch', datetime.datetime(2020, 5, 12, 10, 30, 36, 280000), 1)

    def test_vcs_pull_request_ctor_false_pull_request_id_input(self):
        with self.assertRaises(ValueError):
            VCSPullRequest('bla', 'My new branch', datetime.datetime(2020, 5, 12, 10, 30, 36, 280000), 1, 'True',
                           'my-new-branch', 'master', 5)
        with self.assertRaises(TypeError):
            VCSPullRequest(None, 'My new branch', datetime.datetime(2020, 5, 12, 10, 30, 36, 280000), 1, 'True',
                           'my-new-branch', 'master', 5)

    def test_vcs_pull_request_ctor_false_pull_request_creation_date_input(self):
        with self.assertRaises(TypeError):
            VCSPullRequest(1, 'My new branch', datetime.datetime(None, 5, 12, 10, 30, 36, 280000), 1, 'True',
                           'my-new-branch', 'master', 5)
        with self.assertRaises(TypeError):
            VCSPullRequest(1, 'My new branch', datetime.datetime(None, 5, 12, 10, 30, 36, None), 1, 'True',
                           'my-new-branch', 'master', 5)
        with self.assertRaises(TypeError):
            VCSPullRequest(1, 'My new branch', datetime.datetime(), 1, 'True',
                           'my-new-branch', 'master', 5)
        with self.assertRaises(TypeError):
            VCSPullRequest(1, 'My new branch', 123, 1, 'True',
                           'my-new-branch', 'master', 5)
        with self.assertRaises(TypeError):
            VCSPullRequest(1, 'My new branch', "abc", 1, 'True',
                           'my-new-branch', 'master', 5)

    def test_vcs_pull_request_ctor_false_pull_request_repository_id_input(self):
        with self.assertRaises(ValueError):
            VCSPullRequest(1, 'My new branch', datetime.datetime(2020, 5, 12, 10, 30, 36, 280000), 1, 'True',
                           'my-new-branch', 'master', "abc")
        with self.assertRaises(TypeError):
            VCSPullRequest(1, 'My new branch', datetime.datetime(2020, 5, 12, 10, 30, 36, 280000), 1, 'True',
                           'my-new-branch', 'master', None)


class TestVCSCommit(unittest.TestCase):
    def test_vcs_commit_ctor_missing_input_arguments(self):
        with self.assertRaises(TypeError):
            VCSCommit()
        with self.assertRaises(TypeError):
            VCSCommit('abc')
        with self.assertRaises(TypeError):
            VCSCommit('abc', 'abc')
        with self.assertRaises(TypeError):
            VCSCommit('abc', 'abc', 'abc')
        with self.assertRaises(TypeError):
            VCSCommit('abc', 'abc', 'abc', 2)

    def test_vcs_commit_ctor_false_commit_id_input(self):
        with self.assertRaises(TypeError):
            VCSCommit(None, 'abc', 'abc', 2, 5)

    def test_vcs_commit_ctor_false_commit_repository_id_input(self):
        with self.assertRaises(ValueError):
            VCSCommit('abc', 'abc', 'abc', "bla", 5)
        with self.assertRaises(TypeError):
            VCSCommit('abc', 'abc', 'abc', None, 5)

    def test_vcs_commit_ctor_false_commit_pull_request_id_input(self):
        with self.assertRaises(ValueError):
            VCSCommit('abc', 'abc', 'abc', 2, "bla")
