#! /usr/bin/env python

from requests.exceptions import ConnectionError
from atlassian import Bitbucket as BitbucketSession  # atlassian api tool for python
import pprint  # This is printing json nicely
import requests
import datetime
from exporterutilities.DbMannager import DbMannager
from vcs_exporter.vcs import VCSTool, VCSProject, VCSRepository, VCSCommit, VCSPullRequest

BITBUCKET_URL = 'http://52.169.187.138:7990/'
BITBUCKET_USER = 'admin'
BITBUCKET_PASSWORD = 'admin'


class Bitbucket(VCSTool):
    __instance = None

    def __init__(self):
        if Bitbucket.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            super(Bitbucket, self).__init__()
            self.set_session(BitbucketSession(url=BITBUCKET_URL, username=BITBUCKET_USER, password=BITBUCKET_PASSWORD))
            Bitbucket.__instance = self

    @staticmethod
    def get_instance():
        """ Static access method. """
        if Bitbucket.__instance is None:
            Bitbucket()
        return Bitbucket.__instance

    def fetch_projects(self):
        try:
            projects_data = self.get_session().project_list()
            if len(projects_data) > 0:
                self.__parse_projects_data(projects_data)
            else:
                raise ConnectionError("Connection to Bitbucket was not successful")
        except ValueError as projectError:
            print("Failed to initialize a project, Error: %", projectError)

    def __parse_projects_data(self, projects_data):
        for project in projects_data:
            self.insert_project(VCSProject(project["id"], project["key"], project["name"]))

    def fetch_repositories(self):
        """"Returns all bitbucket`s repos via list projects."""
        try:
            projects = self.get_projects()
            if len(projects) == 0:
                self.fetch_projects()
            for project in projects:
                repositories_data = self.get_session().repo_list(project[1])
                self.__parse_repositories_data(repositories_data)
        except ValueError as projectError:
            print("Failed to initialize a project, Error: %", projectError)

    def __parse_repositories_data(self, repositories_data):
        for repository in repositories_data:
            self.insert_repository(VCSRepository(repository["id"], repository["name"], repository["slug"],
                                                 repository["project"]["id"]))

    def fetch_pull_requests(self):
        try:
            repositories = self.get_repositories()
            if len(repositories) == 0:
                self.fetch_repositories()
            for repository in repositories:
                # making the request
                project_key = self.__get_project_key(repository[2])
                request = requests.get(self.get_session().url + "rest/api/1.0/projects/" + str(project_key) +
                                       "/repos/" + repository[3] + "/pull-requests", auth=(self.get_session().username,
                                                                                           self.get_session().password))
                # converting to json
                pull_requests_data = request.json()
                self.__parse_pull_requests_data(pull_requests_data, repository[0])
        except ValueError as projectError:
            print("Failed to initialize a project, Error: %", projectError)

    def __parse_pull_requests_data(self, pull_requests_data, repository_id):
        if 'size' in pull_requests_data != {} and pull_requests_data["size"] > 0:
            for pull_request in pull_requests_data["values"]:
                # converting ms to Date
                pull_request_creation_date = datetime.datetime.utcfromtimestamp(pull_request["createdDate"] / 1000)
                self.insert_pull_request(VCSPullRequest(pull_request["id"], pull_request["title"],
                                                        pull_request_creation_date,
                                                        pull_request["author"]["user"]["id"], pull_request["open"],
                                                        pull_request["fromRef"]["displayId"],
                                                        pull_request["toRef"]["displayId"], repository_id))

    def __get_project_key(self, project_id):
        for project in self.get_projects():
            if project[0] == project_id:
                return project[1]

    def fetch_commits(self):
        try:
            repositories = self.get_repositories()
            if len(repositories) == 0:
                self.fetch_repositories()
            for repository in repositories:
                # making the request
                project_key = self.__get_project_key(repository[2])
                request = requests.get(self.get_session().url + "rest/api/1.0/projects/" + project_key +
                                       "/repos/" + repository[3] + "/commits?&merges=only&limit=10000",
                                       auth=(self.get_session().username, self.get_session().password))
                # converting to json
                commits_data = request.json()
                self.__parse_commits_data(commits_data, project_key, repository[0], repository[3])
        except ValueError as projectError:
            print("Failed to initialize a project, Error: %", projectError)

    def __parse_commits_data(self, commits_data, project_key, repository_id, repository_slug):
        if 'size' in commits_data and commits_data["size"] > 0:
            for commit in commits_data["values"]:
                commit_pull_request = self.__get_commit_pull_request(commit["id"], project_key, repository_slug)
                self.insert_commit(VCSCommit(commit["id"], commit["author"]["name"], commit["message"],
                                             repository_id, commit_pull_request))

    def __get_commit_pull_request(self, commit_id, project_key, repository_slug):
        # fetching commit's pull request
        request = requests.get(
            self.get_session().url + "rest/api/1.0/projects/" + project_key + "/repos/" + repository_slug +
            "/commits/" + commit_id + "/pull-requests",
            auth=(self.get_session().username, self.get_session().password))
        # converting to json
        pull_request = request.json()
        if pull_request["size"] > 0:
            return pull_request["values"][0]["id"]
        else:
            return None


def main():
    try:
        bitbucket = Bitbucket.get_instance()
        if isinstance(bitbucket, Bitbucket):
            print("**Fetching Projects**")
            bitbucket.fetch_projects()
            print(bitbucket.get_projects())
            print("**Fetching Repositories**")
            bitbucket.fetch_repositories()
            print(bitbucket.get_repositories())
            print("**Fetching Pull Requests**")
            bitbucket.fetch_pull_requests()
            print(bitbucket.get_pull_requests())
            print("**Fetching Commits**")
            bitbucket.fetch_commits()
            print(bitbucket.get_commits())
        # sending all info to postgresql
        dbm = DbMannager()
        dbm.postgressql_execute("bitbucket_projects", bitbucket.get_projects())
        dbm.postgressql_execute("bitbucket_repos", bitbucket.get_repositories())
        dbm.postgressql_execute("bitbucket_pull_requests", bitbucket.get_pull_requests())
        dbm.postgressql_execute("bitbucket_commits", bitbucket.get_commits())

        dbm.close_connection()
    except ConnectionError as e:
        print(e)


if __name__ == '__main__':
    main()
