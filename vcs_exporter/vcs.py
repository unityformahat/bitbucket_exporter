from abc import ABC, abstractmethod
import datetime


class VCSProject:
    def __init__(self, project_id, project_key, project_name):
        self.__project_id = int(project_id)
        self.__project_key = str(project_key)
        self.__project_name = str(project_name)

    def get_project_id(self):
        return self.__project_id

    def get_project_key(self):
        return self.__project_key

    def get_project_name(self):
        return self.__project_name


class VCSRepository:
    def __init__(self, repository_id, repository_name, repository_slug, repository_project_id):
        self.__repository_id = int(repository_id)
        self.__repository_name = str(repository_name)
        self.__repository_slug = str(repository_slug)
        self.__repository_project_id = int(repository_project_id)

    def get_repository_id(self):
        return self.__repository_id

    def get_repository_name(self):
        return self.__repository_name

    def get_repository_slug(self):
        return self.__repository_slug

    def get_repository_project_id(self):
        return self.__repository_project_id


class VCSPullRequest:
    def __init__(self, pull_request_id, pull_request_title, pull_request_creation_date, pull_request_author_id,
                 pull_request_status, pull_request_source_branch, pull_request_target_branch, pull_request_repo_id):
        self.__pull_request_id = int(pull_request_id)
        self.__pull_request_title = str(pull_request_title)
        if isinstance(pull_request_creation_date, datetime.datetime):
            self.__pull_request_creation_date = pull_request_creation_date
        else:
            raise TypeError("creation date must be in datetime format")
        self.__pull_request_author_id = str(pull_request_author_id)
        self.__pull_request_status = str(pull_request_status)
        self.__pull_request_source_branch = str(pull_request_source_branch)
        self.__pull_request_target_branch = str(pull_request_target_branch)
        self.__pull_request_repository_id = int(pull_request_repo_id)

    def get_pull_request_id(self):
        return self.__pull_request_id

    def get_pull_request_title(self):
        return self.__pull_request_title

    def get_pull_request_creation_date(self):
        return self.__pull_request_creation_date

    def get_pull_request_author_id(self):
        return self.__pull_request_author_id

    def get_pull_request_status(self):
        return self.__pull_request_status

    def get_pull_request_source_branch(self):
        return self.__pull_request_source_branch

    def get_pull_request_target_branch(self):
        return self.__pull_request_target_branch

    def get_pull_request_repository_id(self):
        return self.__pull_request_repository_id


class VCSCommit:
    def __init__(self, commit_id, commit_author_name, commit_message, commit_repo_id, commit_pull_request_id):
        if commit_id is not None:
            self.__commit_id = str(commit_id)
        else:
            raise TypeError("commit id should not be None")
        self.__commit_author_name = str(commit_author_name)
        self.__commit_message = str(commit_message)
        self.__commit_repository_id = int(commit_repo_id)
        if isinstance(commit_pull_request_id, int) or commit_pull_request_id is None:
            self.__commit_pull_request_id = commit_pull_request_id
        else:
            raise ValueError("commit id should be an integer or None")

    def get_commit_id(self):
        return self.__commit_id

    def get_commit_author_name(self):
        return self.__commit_author_name

    def get_commit_message(self):
        return self.__commit_message

    def get_commit_repository_id(self):
        return self.__commit_repository_id

    def get_commit_pull_request_id(self):
        return self.__commit_pull_request_id


class VCSTool(ABC):
    def __init__(self, projects=[], repos=[], pull_requests=[], commits=[], session=None):
        self.__projects = list(projects)
        self.__repositories = list(repos)
        self.__pull_requests = list(pull_requests)
        self.__commits = list(commits)
        self.__session = session

    def get_projects(self):
        """"Returns all projects via list object."""
        return self.__projects

    def get_repositories(self):
        """"Returns all repositories via list object."""
        return self.__repositories

    def get_pull_requests(self):
        """"Returns all pull requests via list projects."""
        return self.__pull_requests

    def get_commits(self):
        """"Returns all commits via list projects."""
        return self.__commits

    def get_session(self):
        """"Returns connection session"""
        return self.__session

    def set_session(self, session):
        self.__session = session

    def insert_project(self, project=None):
        """Inserts a project to the project list"""
        if isinstance(project, VCSProject):
            self.__projects.append((project.get_project_id(), project.get_project_key(), project.get_project_name()))
        else:
            raise TypeError("project should be a VCSProject")

    def insert_repository(self, repository=None):
        """Inserts a repository to the repository list"""
        if isinstance(repository, VCSRepository):
            self.__repositories.append((repository.get_repository_id(), repository.get_repository_name(),
                                        repository.get_repository_project_id(), repository.get_repository_slug()))
        else:
            raise TypeError("project should be a VCSRepository")

    def insert_pull_request(self, pull_request=None):
        """Inserts a pull request to the pull request list"""
        if isinstance(pull_request, VCSPullRequest):
            self.__pull_requests.append((pull_request.get_pull_request_id(), pull_request.get_pull_request_title(),
                                         pull_request.get_pull_request_creation_date(),
                                         pull_request.get_pull_request_author_id(),
                                         pull_request.get_pull_request_status(),
                                         pull_request.get_pull_request_source_branch(),
                                         pull_request.get_pull_request_target_branch(),
                                         pull_request.get_pull_request_repository_id()))
        else:
            raise TypeError("project should be a VCSPullRequest")

    def insert_commit(self, commit=None):
        """Inserts a commit to the commit list"""
        if isinstance(commit, VCSCommit):
            self.__commits.append((commit.get_commit_id(), commit.get_commit_author_name(),
                                   commit.get_commit_message(), commit.get_commit_repository_id(),
                                   commit.get_commit_pull_request_id()))
        else:
            raise TypeError("project should be a VCSCommit")

    @abstractmethod
    def fetch_projects(self):
        pass

    @abstractmethod
    def fetch_repositories(self):
        pass

    @abstractmethod
    def fetch_pull_requests(self):
        pass

    @abstractmethod
    def fetch_commits(self):
        pass

