The VCS exporter is a one of numerous data exporters that are meant to fetch development process related data. 
This exporter fetches the following data from a VCS tool:

* Projects
* Repositories
* Pull Requests
* Commits

Currently the exporter is fetching from the following VCS tools:
* Bitbucket

The data is inserted into a PostgreSQL DB, and used later by the client service in Unity.